#pragma once
#include <string>
class GBaseApp
{
public:
	GBaseApp():width(640),height(480),title("GEngine") {}
	virtual ~GBaseApp() {}
	virtual void Init();
	virtual void PrepareRender();
	virtual void AfterRender();
	virtual void Render(int deltaMillionSeconds) {}
	virtual void Release() {}
	//λ�ù̶���
	virtual void MouseEvent(int key,int state,int x,int y) {}
	virtual void MouseWheelEvent(int button, int dir, int x, int y) {}
	virtual void KeyEvent(unsigned char ch, int x, int y) {}
	virtual void SpecialKeyEvent(int ch, int x, int y) {}
	virtual void Reshape(int w, int h) {}
	virtual void MouseMoveEvent(int x, int y) {}
	float width;
	float height;
	std::string title;
};
extern GBaseApp* app;