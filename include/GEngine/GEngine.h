#pragma once
#include <string>
#include <vector>
using I8 = char;
using UI8 = unsigned char;
using I32 = int;
using UI32 = unsigned int;
using I64 = long long;
using UI64 = unsigned long long;
using F32 = float;
using F64 = double;

using String = std::string;
using WideString = std::wstring;

class Object
{
public:
	virtual std::string GetClassName() { return ""; }
	virtual bool IsSameType(Object* obj)
	{
		if (GetClassName() == obj->GetClassName())
		{
			return true;
		}
		return false;
	}

private:
};
class IEngineFunction : public Object
{
public:
	virtual void Release() = 0;
	virtual	std::string GetID() { return resourceID; }
	virtual void SetID(std::string id) { resourceID = id; }
private:
	std::string resourceID;
};

class ITexture : public IEngineFunction
{
public:
	virtual std::string GetClassName() { return "ITextrue"; }
	virtual bool LoadFromFile1(std::string file) = 0;
	virtual bool LoadFromFile2(std::string file) = 0;
	virtual UI32 GetTexId() = 0;
	virtual void Bind() = 0;

};
//定点描述符，用于IVeretxStructArray的属性指定
class IVertexDescriptror : public IEngineFunction
{
public:
	virtual std::string GetClassName() { return "IVertex"; }
	/**
	 * 获取所有属性数量
	 */
	virtual int GetAttribCount() = 0;
	/**
	 * 获取第i个属性的偏移地址 例如：子类可用offsetof(IVertex,Pos)
	 */
	virtual int GetAttribOffset(int i) = 0;
	/**
	 * 描述Vertex对象自身所占多少字节 继承实现可直接用sizeof(Vertex)
	 */
	virtual int GetSelfMemSize() = 0;


};;

class IVerexBufferArray
{
public:
	virtual std::string GetClassName() { return "IVertexBufferArray"; }
	virtual void InitPosition(F32* Color) = 0;
	virtual void InitColor(F32* Color) = 0;
	virtual void InitTexCoord(F32* Color) = 0;
	virtual void InitNormal(F32* Color) = 0;
	virtual void InitEbo(UI32* indexArray, int count) = 0;
	virtual void Draw() = 0;

};

class IVertexArray : public IEngineFunction {
public:
	virtual std::string GetClassName() { return "IVertexArray"; }
	//颜色
	virtual void Init(F32* pos, F32* color, I32 pointCount) = 0;
	//纹理坐标
	virtual void InitWithTexture(F32* pos, F32* tex, I32 pointCount) = 0;
	//位置 纹理 法线
	virtual void InitWithPNT(F32* pos, F32* tex, F32* normal, I32 pointCount) = 0;
	//GL_LINE GL_TRIANGLES GL_TRAINGLE_STRIP,GL_QUAD like Opengl
	virtual void Draw(UI32 mode) = 0;

	virtual void CreateEbo(UI8* v, int count) = 0;
	virtual void InitEbo(UI32* indexArray, int count) = 0;
};

class IRenderTexture : public IEngineFunction
{
public:
	virtual std::string GetClassName() { return "IRenderTexture"; }
	virtual void Create(int w, int h) = 0;
	virtual void Bind() = 0;
	virtual void UnBind() = 0;
	virtual UI32 GetColorTexture() = 0;
	virtual UI32 GetDepthTexture() = 0;
	virtual void BindDepthTexture() = 0;
	virtual void BindColorTexture() = 0;
};
class IShader : public IEngineFunction {
public:
	virtual std::string GetClassName() { return "IShader"; }
	//type = 0 source,type = 1 file
	virtual void LoadShaderSource(std::string vertSource, std::string fragSource) = 0;
	virtual void LoadShaderFile(std::string vertFile, std::string fragFile) = 0;
	virtual void LoadShader(UI32 shaderId, std::string source, I32 type) = 0;

	virtual void Bind() = 0;
	virtual void UnBind() = 0;
	//不支持 UniformBuffers，所以UniformBuffers需要单独列出
	virtual void InitUniformParameters(std::initializer_list<std::string> paras) = 0;
	virtual void SetUniformPara(const char* name) = 0;
	//加载完shader 以及uniform buffer之后调用此函数
	virtual void CreateProgram() = 0;
	//获取相关uniform variables address
	virtual	I32 operator [](const char* name) = 0;
	virtual void SetUniformBlock(const char* name, I32 size) = 0;
	//以下函数都可以把数据更新到shader中在此之前需要Bind
	//设置uniform block
	virtual void SetUniformBlockData(const char* name, void* data, I32 size, I32 binding) = 0;
	virtual void SetUniform1f(std::string name, float val) = 0;
	virtual void SetUniform1i(std::string name, int val) = 0;
	virtual void SetUniform2f(std::string name, float f1, float f2) = 0;
	virtual void SetUniform3f(std::string name, float f1, float f2, float f3) = 0;
	virtual void SetUniform4f(std::string name, float f1, float f2, float f3, float f4) = 0;
	virtual void SetUniformMatrix4fv(std::string name, float* val) = 0;
	virtual void BindTextureToUnit(UI32 texID, UI32 bindID) = 0;
};

class  ITextDraw : public IEngineFunction
{
public:
	virtual std::string GetClassName() { return "ITextDraw"; }
	virtual void LoadFont(std::string font, I32 h) = 0;
	virtual void Draw(WideString wstr, I32 x, I32 y, I32 maxW, I32 h) = 0;
};

class  IWidget : public IEngineFunction
{
public:
	virtual std::string GetClassName() { return "IWidget"; }
	virtual void SetFont(std::string font, I32 h) = 0;
	virtual void Init(std::string capt, I32 x, I32 y, I32 w, I32 h) = 0;
	virtual double* AddParam(std::string paraName, F64 value, F64 min, F64 max) = 0;
	virtual int* AddParam(std::string paraName, I32 value, I32 min, I32 max) = 0;
	virtual void LoadFromFile(std::string name) = 0;
	virtual void SaveToFile(std::string name) = 0;
	virtual void Draw() = 0;
	virtual void Update() = 0;
	virtual void MouseEvent(I32 state, I32 key, I32 x, I32 y) = 0;
	virtual void MouseMoveEvent(I32 x, I32 y) = 0;
	virtual void MouseWheelEvent(I32 dir, I32 state, I32 x, I32 y) = 0;
	virtual void* GetPara(std::string name) = 0;
};
//这两个还未实现
//针对图像处理的一个管线，可以多层嵌套
//class IAfterEffectShader : public IEngineFunction
//{
//public:
//	virtual std::string GetClassName() { return "IRenderPipeLine"; }
//	virtual void AppendEffectShader(std::string vert, std::string frag) = 0;
//	virtual void SetInputTextureID(UI32 tex, UI32 x, UI32 y) = 0;
//	virtual UI32 GetOutputTexture() = 0;
//
//};
//class IRenderPipeLine : public IEngineFunction
//{
//public:
//	virtual std::string GetClassName() { return "IRenderPipeLine"; }
//	virtual void SetUseDefferedRender(bool bDeffered) = 0;
//	virtual void SetViewPort(UI32 x, UI32 y) = 0;
//	virtual void SetAfterEffectShader(std::string vert, std::string frag) = 0;
//	//fbo绑定
//	virtual void BeginRendering() = 0;
//	//切换到屏幕上
//	virtual void AfterRendering() = 0;
//
//};

class EngineBuilder
{
public:
	~EngineBuilder();
	ITexture* NewTexture(std::string id = "");
	IVertexArray* NewVertexArray(std::string id = "");
	IRenderTexture* NewRenderTexture(std::string id = "");
	ITextDraw* NewTextDraw(std::string id = "");
	IShader* NewShader(std::string id = "");
	IWidget* NewWidget(std::string id = "");

private:
	std::vector<IEngineFunction*> objs;

};

class RenderContext
{
public:
	static RenderContext& GetInst()
	{
		static RenderContext inst;
		return inst;
	}
	enum Mode
	{
		RC_DEPTH = 0x1801,
		RC_DEPTH_TEST = 0x0B71,
		RC_ALPHA = 0x1906,
		RC_ALPHA_TEST = 0x0BC0,
		RC_TEXTURE = 0x1702,
		RC_TEXTURE_2D = 0x0DE1,
		RC_LESS = 0X0201,
		RC_EQUAL = 0X0202,
		RC_GREATER = 0X0204,

		//shape
		RC_LINES = 0x0001,
		RC_LINE_LOOP = 0x0002,
		RC_TRIANGLES = 0x0004,
		RC_TRIANGLE_STRIP = 0x0005,
		RC_TRIANGLE_FAN = 0x0006,
		RC_QUADS = 0x0007

	};

	void Enable(RenderContext::Mode mode);
	void Disable(RenderContext::Mode mode);
	void DepthFunc(RenderContext::Mode mode);
	void ClearColor(F32 r, F32 g, F32 b, F32 a);
	void ClearDepth(int depth);
	void ClearColorBuffer();
	void ClearDepthBuffer();
	void ViewPort(int x, int y, int w, int h);
	void Flush();
	void SwapBuffer();
	void BindTexture(UI32 texID);
	void Convert2DDraw(int left, int right, int top, int bottom);
	void ConvertPerspective(float fov, float aspect, float near, float far);

};

#define RCObj RenderContext::GetInst()
#define RC(FUNC) RenderContext::GetInst().##FUNC()
#define RC1(FUNC,arg1) RenderContext::GetInst().##FUNC## (arg1)

#define RC2(FUNC,arg1,arg2) RenderContext::GetInst().##FUNC##(arg1,arg2)

#define RC3(FUNC,arg1,arg2,arg3) RenderContext::GetInst().##FUNC##(arg1,arg2,arg3)

#define RC4(FUNC,arg1,arg2,arg3,arg4) RenderContext::GetInst().##FUNC##(arg1,arg2,arg3,arg4)


extern int RunGEngine(int argc, char** argv);


template<class T>
extern T* RegisterApp();
