#include "DemoVertexArray_App.h"

static F32 posDemo[] = { -1.f, -1.f, 0.1f,   1., -1.f, 0.1f,
					1.f, 1.f ,0.1f ,  -1.f, 1.f, 0.1f };
static F32 colorDemo[] = { 0.f, 1.f, 0.f,   1.f, 0.f, 0.f,
					1.f, 0.f, 0.f,  1.f, 1.f, 0.f };

static F32 posCube[] = { -50, 0.3f, 50.f,    50.f,0.3f, 50.f,
			50.f, 0.3f, -50.f,   -50.f,0.3f, -50.f };

static F32 texCube[] = { 0.,0.,  1.f, 0.f,  1.f, 1.f,  0.f,1.0f };
static F32 normalCube[] = { 1.f,1.f,1.f, 1.f, 1.f,1.f,  1.f,1.f,1, 1.f,1.f,1.f };

void DemoVertexArray_App::Init()
{
	//RCObj.Enable(RenderContext::Mode::RC_DEPTH_TEST);
	RC1(Enable, RenderContext::Mode::RC_DEPTH_TEST);
	RCObj.Enable(RenderContext::RC_TEXTURE_2D);
	shaderDemo = builder.NewShader();
	vaDemo = builder.NewVertexArray();
	tex = builder.NewTexture();
	widget = builder.NewWidget();

	shaderDemo->LoadShaderFile("shader/va.vert", "shader/va.frag");
	shaderDemo->InitUniformParameters({ "proj","view","trans" });
	//vaDemo->Init(posDemo, colorDemo, 4);
	vaDemo->InitWithPNT(posCube, texCube, normalCube, 4);
	width = 640;
	height = 480;
	tex->LoadFromFile1("stone.jpeg");

	widget->Init("Set", 10, 10, 300, 600);
	widget->SetFont("simhei.ttf", 14);
	x = widget->AddParam("transX", 5.0, 0.0, 30.0);
	y = widget->AddParam("transY", 5.0, 0.0, 30.0);
	z = widget->AddParam("transZ", 5.0, 0.0, 30.0);
	proj = glm::perspective(glm::radians(60.f),(float) (width / height), 0.1f, 2000.f);
	view = glm::lookAt(vec3(-10, -10, -10), vec3(0, 0, 0), vec3(0,0,1));
}

void DemoVertexArray_App::Render(int deltaMillionSeconds)
{
	RC1(DepthFunc, RenderContext::Mode::RC_LESS);
	RC4(ClearColor, 0., 0., 0., 1);
	shaderDemo->Bind();
	trans = glm::translate(vec3(*x, *y, *z));
	shaderDemo->SetUniformMatrix4fv("trans", value_ptr(trans));
	shaderDemo->SetUniformMatrix4fv("view", value_ptr(view));
	shaderDemo->SetUniformMatrix4fv("proj", value_ptr(proj));
	tex->Bind();
	vaDemo->Draw(RenderContext::Mode::RC_QUADS);
	shaderDemo->UnBind();
	RCObj.GetInst().Convert2DDraw(0, width, 0, height);
	RCObj.ClearDepthBuffer();
	RCObj.ClearDepth(100);
	widget->Update();
	widget->Draw();

}

void DemoVertexArray_App::Reshape(int w, int h)
{
	width = w;
	height = h;
	RC4(ViewPort, 0, 0, w, h);
}

void DemoVertexArray_App::AfterRender()
{
	Sleep(200);
	RCObj.Flush();
	RCObj.SwapBuffer();
}

void DemoVertexArray_App::MouseEvent(int key, int state, int x, int y)
{
	widget->MouseEvent(state, key, x, y);
}

void DemoVertexArray_App::MouseWheelEvent(int button, int dir, int x, int y)
{
	widget->MouseWheelEvent(button, dir, x, y);
}

void DemoVertexArray_App::MouseMoveEvent(int x, int y)
{
	widget->MouseMoveEvent(x, y);
}
