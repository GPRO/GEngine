#include <fstream>
#include "FirstPersonCamera.h"


AFirstPersonCamera::AFirstPersonCamera()
{


}

AFirstPersonCamera::~AFirstPersonCamera()
{

}

void AFirstPersonCamera::Init(vec3 eye, vec3 center, vec3 up)
{
	originEye = eye;// vec3(0, 0, 0);
	//originCenter = center; //vec3(0, 0, 0);
	//originUp = up;
	originCenter = originEye + vec3(0, 1, 0);
	originUp = vec3(0, 0, 1);
	//originEye = vec3(0, -200, 50);
	//originCenter = vec3(0, 1, 0);
	//originUp = vec3(0, 1, 0);
	angleAlpha = 0;// M_PI / 2;
	angleTheta = 0;
}

void AFirstPersonCamera::LoadConfig()
{
	std::ifstream fs("camera.txt");
	if (fs.is_open())
	{
		fs >> originEye.x >> originEye.y >> originEye.z;
		fs >> originCenter.x >> originCenter.y >> originCenter.z;
		fs >> originUp.x >> originUp.y >> originUp.z;
		fs >> angleAlpha;
		fs >> angleTheta;
		fs.close();
	}
}

void AFirstPersonCamera::MouseMove(int x, int y)
{
	if (isMove)
	{
		float offsetX = x - originMouse.x;
		float offsetY = y - originMouse.y;
		float r = 100000;
		float increaseHorizonAngle = atan(offsetX / r);
		float increaseVerticalAngle = atan(offsetY / r);
		cout << "increaseHorizonAngle:" << increaseHorizonAngle << " increaseVerticalAngle:" << increaseVerticalAngle << endl;
		angleAlpha += increaseHorizonAngle;
		angleTheta += increaseVerticalAngle;
	}
}

void AFirstPersonCamera::MouseDown(int x, int y)
{
	if (!isCapture) return;
	isMove = true;
	originMouse.x = x;
	originMouse.y = y;
}

void AFirstPersonCamera::MouseUp(int x, int y)
{
	isMove = false;
}

void AFirstPersonCamera::Capture(bool bUseCapture)
{
	this->isCapture = bUseCapture;
}

void AFirstPersonCamera::TurnLeft(float speed)
{
	speed = speed / 180.0 * M_PI;
	angleAlpha += speed;

}
void AFirstPersonCamera::TurnRight(float speed)
{
	speed = speed / 180.0 * M_PI;
	angleAlpha -= speed;
}
void AFirstPersonCamera::TurnUp(float speed)
{
	speed = speed / 180.0 * M_PI;
	angleTheta += speed;
}
void AFirstPersonCamera::TurnDown(float speed)
{
	speed = speed / 180.0 * M_PI;
	angleTheta -= speed;
}

void AFirstPersonCamera::MoveLeft(float speed)
{
	originEye.x -= speed * sin(angleAlpha);
	originEye.y += speed * cos(angleAlpha);
}
void AFirstPersonCamera::MoveRight(float speed)
{
	originEye.x += speed * sin(angleAlpha);
	originEye.y -= speed * cos(angleAlpha);
}
void AFirstPersonCamera::MoveForwad(float speed)
{
	//originEye.x += speed * cos(angleAlpha);
	//originEye.y += speed * sin(angleAlpha);
	vec3 dc = normalize(DstCenterDir());
	originEye.x += speed * dc.x;
	originEye.y += speed * dc.y;
}

void AFirstPersonCamera::MoveBack(float speed)
{
	originEye.x -= speed * cos(angleAlpha);
	originEye.y -= speed * sin(angleAlpha);
}

void AFirstPersonCamera::MoveUp(float speed)
{
	originEye.z += speed;
}

void AFirstPersonCamera::MoveDown(float speed)
{
	originEye.z -= speed;

}

vec3 AFirstPersonCamera::DstEye()
{
	return originEye;
}

vec3 AFirstPersonCamera::DstCenterDir()
{
	//vec3 ctr(originEye.x + 1 * cos(angleAlpha),
	//	originEye.y + 1 * sin(angleAlpha), originEye.z + cos (angleTheta));
	//vec3 center( cos(angleAlpha),
	//	sin(angleAlpha), originEye.z);
	vec3 oc(originCenter - originEye);
	return rotateX(glm::rotateZ(oc, -angleAlpha), -angleTheta);
}

vec3 AFirstPersonCamera::DstUp()
{
	//vec3 upDir(0 ,
	//	originEye.y+100 * sin(angleTheta), originEye.z + 100* cos(angleTheta));
	//vec3 upDir(0,
	//	0+ 1 * sin(angleTheta),0 + 1* cos(angleTheta));
	//return upDir;
	vec3 oc(originUp);
	//return rotateX(glm::rotateZ(oc, angleAlpha), angleTheta);
	return oc;
}

void AFirstPersonCamera::Save()
{
	std::ofstream fs("camera.txt");
	if (fs.is_open())
	{
		fs << originEye.x << " " << originEye.y << " " << originEye.z << " ";
		fs << originCenter.x << " " << originCenter.y << " " << originCenter.z << " ";
		fs << originUp.x << " " << originUp.y << " " << originUp.z << " ";
		fs << angleAlpha << " ";
		fs << angleTheta;
		fs.flush();
		fs.close();
	}
}
