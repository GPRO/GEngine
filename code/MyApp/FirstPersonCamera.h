#pragma once
#include "MyHeader.h"
class AFirstPersonCamera
{
public:
	AFirstPersonCamera();
	~AFirstPersonCamera();
	void Init(vec3 eye, vec3 center, vec3 up);
	void LoadConfig();
	void MouseMove(int x,int y);
	void MouseDown(int x,int y);
	void MouseUp(int x,int y);
	void Capture(bool bUseCapture);
	bool IsCapture() { return isCapture; }
	void TurnLeft(float speed);
	void TurnRight(float speed);
	void TurnUp(float speed);
	void TurnDown(float speed);

	void MoveLeft(float left);
	void MoveRight(float right);
	void MoveForwad(float up);
	void MoveBack(float down);
	void MoveUp(float);
	void MoveDown(float);
	void PrintInfo()
	{
		/*cout << "center" << sin(angleAlpha) << "," << cos(angleAlpha) << ",0" << endl;
		cout << "up:0," << sin(angleTheta) << "," << cos(angleTheta) << endl;*/
		vec3 dcenter = DstCenterDir();
		vec3 pos = DstEye();
		vec3 up = DstUp();
		PrintInfo("dceneter", dcenter);
		PrintInfo("pos", pos);
		PrintInfo("up", up);
	}
	void PrintInfo(string info, vec3 v)
	{
		cout.precision(2);
		cout << info.data() << ":(" << v.x << "," << v.y << "," << v.z << ")" << endl;
	}
	vec3 DstEye();
	vec3 DstCenterDir();
	vec3 DstUp();
	mat4 GetLookMatrix()
	{
		mat4 view = glm::lookAt(DstEye(), DstEye()+DstCenterDir(), DstUp());
		return view;
	}
	void Save();
private:
	vec3 originEye;
	vec3 originCenter;
	vec3 originUp;
	float angleAlpha;	//水平面夹角	影响eye前进 影响center
	float angleTheta;	//垂直面夹角 影响 center 和up

	float M_PI{ 3.1415926 };
	bool isCapture{false};
	bool isMove{ false };

	vec2 originMouse;

};