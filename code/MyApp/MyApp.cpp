﻿// MyApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include "MyHeader.h"
#include "DemoVertexArray_App.h"
#include "ShadowMap_App.h"
#include "ImageCoord_App.h"

using namespace std;

int main(int argc, char** argv)
{

	/*cout << "1:DemoVertexArray_App" << endl;
	cout << "请输入数字";*/

	GBaseApp* myapp = nullptr;
	int n = 3;
	switch (n)
	{

	case 1:
		myapp = new DemoVertexArray_App();
		break;
	case 2:
		myapp = new ShadowMap_App();
		break;
	case 3:
		myapp = new ImageCoord_App();
		break;
	default:
		break;
	}

	myapp->width = 640;
	myapp->width = 480;
	app = myapp;
	RunGEngine(argc, argv);
	if (myapp != nullptr)
	{
		delete myapp;
	}
	return 0;
}
