#pragma once
#include "MyHeader.h"
#include "FirstPersonCamera.h"
/**
 * shadow map注意点，使用的color存储深度值用到了深度和颜色的互相转换
 * 开启深度检测
 * shadow贴图需要默认clearcolor为白色（即最大深度值）
 * 开启深度并且depthFunc为depth_less
 * 输出到shadowmap的 范围实际上是【-1,1】纹理存储范围实际上是【0,1】需要映射
 */
class ShadowMap_App : public GBaseApp
{

public:
	ShadowMap_App() :GBaseApp() {}
	~ShadowMap_App();
	virtual void Init() override;
	virtual void Render(int deltaMillionSeconds) override;
	void RenderObject(int mode, IShader* shader);
	virtual void Release() override;
	virtual void PrepareRender() override;
	void AfterRender()
	{
		RCObj.Flush();
		RCObj.SwapBuffer();
	}

	virtual void MouseEvent(int key, int state, int x, int y) override;
	virtual void MouseWheelEvent(int button, int dir, int x, int y) override;
	virtual void KeyEvent(unsigned char ch, int x, int y) override;
	virtual void SpecialKeyEvent(int ch, int x, int y) override;
	virtual void Reshape(int w, int h) override;
	virtual void MouseMoveEvent(int x, int y) override;

	void UpdateUniformParas(IShader* shade, int mode);
	mat4 GetViewMatrix()
	{
		mat4 camMatrix;
		if (bUseCam)
		{

			camMatrix = fstCamara.GetLookMatrix(); 
			myTexShader->SetUniformMatrix4fv("view", glm::value_ptr(camMatrix));
		}
		else {
			camMatrix = glm::lookAt(vec3(viewPos.x, viewPos.y, viewPos.z), vec3(*centerPos[0], *centerPos[1], *centerPos[2]), vec3(0, 0, 1));
			myTexShader->SetUniformMatrix4fv("view", glm::value_ptr(camMatrix));
		}
		return camMatrix;
	}
private:
	IVertexArray* vaDemo;
	IVertexArray* vaGround;
	IVertexArray* vaCube;
	ITexture* tex;
	IShader* shadowMapShader;
	IShader* shaderDemo;
	mat4x4 texModel;
	double* texModelTranslate[3];
	double* depthTestValue;
	IShader* myTexShader;
	IWidget* widget;
	IRenderTexture* shadowTexture;

	AFirstPersonCamera fstCamara;

	vec3 vlightPos;	//point light

	mat4x4 modelMatrix;
	mat4x4 view;
	mat4x4 proj;
	vec3 viewPos{ 0,0,0 };
	vec3 center{ 100,100,0 };
	vec2 size;

	double* eyePos[3];
	double* centerPos[3];
	double* lightPos[3];
	EngineBuilder builder;
	bool bUseCam = false;
};