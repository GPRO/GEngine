#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include "GEngine/GBaseApp.h"
#include "GEngine/GEngine.h"
#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <windows.h>
#pragma comment(lib,"GEngine.lib")

using namespace glm;
using namespace std;