#pragma once
#include "MyHeader.h"
class DemoVertexArray_App : public GBaseApp
{
public:
	virtual void Init() override;
	virtual void Render(int deltaMillionSeconds) override;
	virtual void Reshape(int w, int h) override;
	virtual void AfterRender() override;


	virtual void MouseEvent(int key, int state, int x, int y) override;


	virtual void MouseWheelEvent(int button, int dir, int x, int y) override;


	virtual void MouseMoveEvent(int x, int y) override;

private:
	IVertexArray* vaDemo;
	IShader* shaderDemo;
	ITexture* tex;
	EngineBuilder builder;
	IWidget* widget;
	mat4 trans;
	mat4 proj;
	mat4 view;
	double *x, *y, *z;

	int* iv;
};