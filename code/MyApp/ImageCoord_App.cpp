#include "ImageCoord_App.h"
F32 posRect[] = { -1,1,-1, 1,1,-1, 1,-1,-1,-1,-1,-1, };
F32 texRect[] = { 0,0, 0,1, 1,1, 1,0 };
F32 nmRect[] = { 0.2,-1,1, 0.4,1,1, 0.6,1,1, 0.8,-1,1 };
std::vector<F32> vt;
std::vector<F32> pos;
vec3 clrWhite(1,1,1);
vec3 clrBlack(0,0,0);
vec3 clrLightGray(0.3,0.3,0.3);
vec3 clrDarkGray(0.6,0.6,0.6);
std::vector<vec3> bkColor;
void ImageCoord_App::Init()
{
	//RCObj.Enable(RenderContext::Mode::RC_DEPTH_TEST);
	RC1(Enable, RenderContext::Mode::RC_DEPTH_TEST);
	RCObj.Enable(RenderContext::RC_TEXTURE_2D);
	shaderDemo = builder.NewShader();
	vaDemo = builder.NewVertexArray();
	tex = builder.NewTexture();
	widget = builder.NewWidget();
	widget->Init("Set", 10, 10, 300, 600);
	widget->SetFont("simhei.ttf", 14);
	s1 = widget->AddParam("s1", 0.0, -0.2, 1.2);
	t1 = widget->AddParam("t1", 1.0, -0.2, 1.2);
	s2 = widget->AddParam("s2", 1.0, -0.2, 1.2);
	t2 = widget->AddParam("t2", 1.0, -0.2, 1.2);
	s3 = widget->AddParam("s3", 1.0, -0.2, 1.2);
	t3 = widget->AddParam("t3", 0.0, -0.2, 1.2);
	s4 = widget->AddParam("s4", 0.0, -0.2, 1.2);
	t4 = widget->AddParam("t4", 0.0, -0.2, 1.2);

	shaderDemo->LoadShaderFile("shader/rect.vert", "shader/rect.frag");
	shaderDemo->InitUniformParameters({ "tex","vt1","vt2","vt3","vt4" });
	//vaDemo->Init(posDemo, colorDemo, 4);
	vt.resize(8);
	GenerateTexCoord(vt);
	pos.resize(12);
	GeneratePos(pos);
	vaDemo->InitWithPNT(pos.data(), vt.data(), nmRect, 4);

	width = 1280;
	height = 640;
	tex->LoadFromFile1("DefaultSkin.png");
	//tex->LoadFromFile1("stone.jpeg");
	bkColor.push_back(clrBlack);
	bkColor.push_back(clrWhite);
	bkColor.push_back(clrLightGray);
	bkColor.push_back(clrDarkGray);

}

void ImageCoord_App::Render(int deltaMillionSeconds)
{
	RC1(DepthFunc, RenderContext::Mode::RC_LESS);
	RC4(ClearColor, bkColor[bkMode].r, bkColor[bkMode].g, bkColor[bkMode].b, 1);
	RCObj.Enable(RenderContext::RC_TEXTURE_2D);
	shaderDemo->Bind();
	RCObj.BindTexture(tex->GetTexId());

	shaderDemo->SetUniform1i("tex", 0);
	shaderDemo->SetUniform2f("vt1", *s1, *t1);
	shaderDemo->SetUniform2f("vt2", *s2, *t2);
	shaderDemo->SetUniform2f("vt3", *s3, *t3);
	shaderDemo->SetUniform2f("vt4", *s4, *t4);
	//shaderDemo->BindTextureToUnit(tex->GetTexId(),0);
	tex->Bind();
	vaDemo->Draw(RenderContext::Mode::RC_QUADS);
	shaderDemo->UnBind();
	RCObj.GetInst().Convert2DDraw(0, width, 0, height);
	RCObj.ClearDepthBuffer();
	RCObj.ClearDepth(100);
	widget->Update();
	widget->Draw();
	cout << deltaMillionSeconds << endl;
}

void ImageCoord_App::Reshape(int w, int h)
{
	RCObj.ViewPort(0, 0, w, h);
}

void ImageCoord_App::AfterRender()
{
	Sleep(200);
	RCObj.Flush();
	RCObj.SwapBuffer();
}

void ImageCoord_App::MouseEvent(int key, int state, int x, int y)
{
	widget->MouseEvent(state, key, x, y);
}

void ImageCoord_App::KeyEvent(unsigned char ch, int x, int y)
{
	if (ch == 's') {
		widget->SaveToFile("ui/1.txt");
	}
	if (ch == 'l') {
		widget->LoadFromFile("ui/1.txt");
	}
	if (ch == 'z') {
		bkMode--;
		if(bkMode < 0)
			 bkMode+=4;
		bkMode %= 4;
	}
	if (ch == 'x') {
		bkMode++;
		bkMode %= 4;
	}
}

void ImageCoord_App::MouseWheelEvent(int button, int dir, int x, int y)
{
	widget->MouseWheelEvent(button, dir, x, y);
}

void ImageCoord_App::MouseMoveEvent(int x, int y)
{
	widget->MouseMoveEvent(x, y);
}

void ImageCoord_App::GenerateTexCoord(std::vector<F32>& vt)
{
	F32 tc[] = {
		*s1,*t1,
		*s2,*t2,
		*s3,*t3,
		*s4,*t4
	};
	for (int i = 0; i < 8; i++) {
		vt[i] = 0;//tc[i];
	}
}

void ImageCoord_App::GeneratePos(std::vector<F32>& pos)
{
	for (int i = 0; i < 12; i++) {
		pos[i] = posRect[i] * 0.4f;
	}
}

