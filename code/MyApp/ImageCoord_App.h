#pragma once
#include "MyHeader.h"
//这是一个uv采样工具，运行后上下左右的UV可以自己调节，然后获得UV数据
//使用的时候直接拿来即可
// z x 切换背景色
class ImageCoord_App : public GBaseApp
{
public:
	virtual void Init() override;
	virtual void Render(int deltaMillionSeconds) override;
	virtual void Reshape(int w, int h) override;
	virtual void AfterRender() override;


	virtual void MouseEvent(int key, int state, int x, int y) override;
	virtual void KeyEvent(unsigned char ch, int x, int y) override;

	virtual void MouseWheelEvent(int button, int dir, int x, int y) override;


	virtual void MouseMoveEvent(int x, int y) override;
	void GenerateTexCoord(std::vector<F32>& vt);
	void GeneratePos(std::vector<F32>& pos);

private:
	IVertexArray* vaDemo;
	IShader* shaderDemo;
	ITexture* tex;
	EngineBuilder builder;
	IWidget* widget;
	double* x, * y, * z;

	double* s1, * t1;
	double* s2, * t2;
	double* s3, * t3;
	double* s4, * t4;

	int bkMode;
};