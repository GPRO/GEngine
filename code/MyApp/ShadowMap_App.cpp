#include "ShadowMap_App.h"



F32 posGround[] = { -200.f, -200.f, 0.f,   200.f, -200.f, 0.f,
					200.f, 200.f, 0.f,   -200.f, 200.f, 0.f };
F32 texGround[] = { 0.,0.,  1.f, 0.f,  1.f, 1.f,  0.f,1.0f };
F32 normalGround[] = { 0.,0.,1.f,   0.,0.,1.f, 0.,0.,1.f,	0.,0.,1.f };

F32 posCube[] = { -50, 0.3f, 50.f,    50.f,0.3f, 50.f,
			50.f, 0.3f, -50.f,   -50.f,0.3f, -50.f };

F32 texCube[] = { 0.,0.,  1.f, 0.f,  1.f, 1.f,  0.f,1.0f };
F32 normalCube[] = { 0.f,1.f,0.f, 0.f,1.f,0.f, 0.f,1.f,0.f, 0.f,1.f,0.f };



ShadowMap_App::~ShadowMap_App()
{
	cout << "release" << endl;
	Release();
}

void ShadowMap_App::Init()
{
	tex = builder.NewTexture("tex");
	vaDemo = builder.NewVertexArray("vaDemo");
	vaCube = builder.NewVertexArray("vaCube");
	vaGround = builder.NewVertexArray("vaGround");

	shadowMapShader = builder.NewShader("shadowMapShader");
	myTexShader = builder.NewShader("myTexShader");
	widget = builder.NewWidget("widget");
	shadowTexture = builder.NewRenderTexture("shadowTexture");
	bUseCam = false;

	{

		tex->LoadFromFile1("stone.jpeg");
		myTexShader->LoadShaderFile("shader/mytext.vert", "shader/mytext.frag");
		myTexShader->InitUniformParameters({ "model","view","proj" });
		shadowMapShader->LoadShaderFile("shader/shadowMap.vert", "shader/shadowMap.frag");
		//全局
		shadowMapShader->InitUniformParameters({ "model","view","proj","viewPos","lightPos","lightView","lightMaxDistance","ambient","diffuse","specular","mode","depthTestValue" });
		shadowTexture->Create(640, 480);
	}
	{
		//光源
		vaGround->InitWithPNT(posGround, texGround, normalGround, 4);
		vaCube->InitWithPNT(posCube, texCube, normalCube, 4);
		RCObj.Enable(RenderContext::RC_TEXTURE_2D);
		RCObj.ClearDepth(1000);
		title = "GEngine-ShadowMap";
		width = 1280;
		height = 720;
		vlightPos = vec3(200, 0, 200);
		proj = glm::perspective<float>(glm::radians(60.f), width / height, .1f, 1000.f);
	}
	{

		widget->Init("Set", 10, 10, 300, 600);
		widget->SetFont("simhei.ttf", 14);
		eyePos[0] = widget->AddParam("eyeX", 0., -200., 200.);
		eyePos[1] = widget->AddParam("eyeY", 0., -200., 200.);
		eyePos[2] = widget->AddParam("eyeZ", 200., -200., 200.);
		fstCamara.Init(vec3(30, 30, 30), vec3(0, 0, 0), vec3(0, 0, 1));
		fstCamara.LoadConfig();
		centerPos[0] = widget->AddParam("centerX", 0., -200., 200.);
		centerPos[1] = widget->AddParam("centerY", 0., -200., 200.);
		centerPos[2] = widget->AddParam("centerZ", 0., -200., 200.);
		texModelTranslate[0] = widget->AddParam("texModelTranslateX", 0., -400., 400.);
		texModelTranslate[1] = widget->AddParam("texModelTranslateY", 0., -400., 400.);
		texModelTranslate[2] = widget->AddParam("texModelTranslateZ", 0., -400., 400.);
		lightPos[0] = widget->AddParam("lightPosX", 0., -400., 400.);
		lightPos[1] = widget->AddParam("lightPosY", 0., -400., 400.);
		lightPos[2] = widget->AddParam("lightPosZ", 200., -400., 400.);
		depthTestValue = widget->AddParam("depthTestValue", 0., -20., 20.);
		widget->LoadFromFile("cam.txt");
	}
	mat4 looka = glm::lookAt(vec3(0, 0, 0), vec3(1, 0, 0), vec3(0, 0, 1));
	mat4 pro = glm::perspective(glm::radians(60.f), 1.f, 0.1f, 1000.f);
	vec4 p(600, 10,0,1);
	vec4 pos = pro * looka * p;
	cout << "pos:"<<pos.x << " " << pos.y << " " << pos.z << " " << pos.w << endl;


}

void ShadowMap_App::Render(int deltaMillionSeconds)
{
	GBaseApp::Render(deltaMillionSeconds);
	RCObj.ViewPort(0, 0, width, height);
	RCObj.ConvertPerspective(60, width / height, 0.1, 1000);
	RCObj.ViewPort(0, 0, width, height);
	RCObj.Enable(RenderContext::RC_TEXTURE_2D);
	RCObj.Enable(RenderContext::Mode::RC_DEPTH);
	RCObj.Enable(RenderContext::Mode::RC_DEPTH_TEST);
	RCObj.DepthFunc(RenderContext::Mode::RC_LESS);
	{
		shadowTexture->Bind();
		shadowMapShader->Bind();
		RCObj.ViewPort(0, 0, 640, 480);
		RCObj.ConvertPerspective(60, width / height, 0.1, 1000);
		RCObj.Enable(RenderContext::Mode::RC_DEPTH);
		RCObj.Enable(RenderContext::Mode::RC_DEPTH_TEST);
		RCObj.DepthFunc(RenderContext::Mode::RC_LESS);
		RCObj.ClearColorBuffer();
		RCObj.ClearColor(1, 1, 1, 1);
		RCObj.ClearColorBuffer();
		RCObj.ClearDepthBuffer();
		RCObj.ClearDepth(1000);
		//RenderShadow
		RenderObject(0, shadowMapShader);
		shadowMapShader->UnBind();
		shadowTexture->UnBind();
	}
	RCObj.ViewPort(0, 0, width, height);
	RCObj.ViewPort(0, 0, width, height);
	RCObj.Enable(RenderContext::RC_TEXTURE_2D);
	RCObj.Enable(RenderContext::Mode::RC_DEPTH);
	RCObj.Enable(RenderContext::Mode::RC_DEPTH_TEST);
	RCObj.DepthFunc(RenderContext::Mode::RC_LESS);
	RCObj.ClearColor(0, 0, 0, 1);

	{	//场景
		shadowMapShader->Bind();
		RenderObject(1, shadowMapShader);
		shadowMapShader->UnBind();
	}
	//---------------------------
	{
		RCObj.ViewPort(0, 0, width, height);
		RCObj.Enable(RenderContext::RC_TEXTURE_2D);
		RCObj.Enable(RenderContext::Mode::RC_DEPTH);
		RCObj.Enable(RenderContext::Mode::RC_DEPTH_TEST);
		RCObj.DepthFunc(RenderContext::Mode::RC_LESS);
		//光源位置输出
		myTexShader->Bind();
		texModel = glm::translate(vec3(*texModelTranslate[0], *texModelTranslate[1], *texModelTranslate[2]));
		myTexShader->SetUniformMatrix4fv("model", glm::value_ptr(texModel));
		myTexShader->SetUniformMatrix4fv("proj", glm::value_ptr(proj));
		//myTexShader->SetUniformMatrix4fv("view", glm::value_ptr(view));
		myTexShader->BindTextureToUnit(shadowTexture->GetColorTexture(), 0);
		myTexShader->SetUniformMatrix4fv("view", const_cast<float*>(glm::value_ptr(GetViewMatrix())));

		vaCube->Draw(RenderContext::RC_QUADS);
		myTexShader->UnBind();
	}
	RCObj.Convert2DDraw(0, width, 0, height);
	widget->Draw();

	viewPos = vec3(*eyePos[0], *eyePos[1], *eyePos[2]);
	center = vec3(*centerPos[0], *centerPos[1], *centerPos[2]);
	vlightPos = vec3(*lightPos[0], *lightPos[1], *lightPos[2]);
}

void ShadowMap_App::RenderObject(int mode, IShader* shader)
{
	UpdateUniformParas(shader, mode);
	if (mode == 1)
	{
		shader->BindTextureToUnit(shadowTexture->GetColorTexture(), 0);
		shadowTexture->BindColorTexture();
	}
	mat4 m;
	m = glm::rotate(m, 45.f, vec3(1, 0, 0)); //, vec3(*texModelTranslate[0], *texModelTranslate[1], *texModelTranslate[2]));
	//glUniformMatrix4fv(shader["model"], 1, GL_FALSE, glm::value_ptr(m));
	shader->SetUniformMatrix4fv("model", glm::value_ptr(m));
	vaCube->Draw(RenderContext::Mode::RC_QUADS);

	UpdateUniformParas(shader, mode);
	shader->SetUniformMatrix4fv("model", glm::value_ptr(modelMatrix));
	vaGround->Draw(RenderContext::Mode::RC_QUADS);
}

void ShadowMap_App::Release()
{
	widget->SaveToFile("cam.txt");
}

void ShadowMap_App::PrepareRender()
{

	RCObj.ViewPort(0, 0, width, height);
	RCObj.ClearColorBuffer();
	RCObj.ClearDepthBuffer();
	RCObj.ClearDepth(1000);
	RCObj.ClearColor(0.4, 0.4, 0.4, 1);

	RCObj.Enable(RenderContext::Mode::RC_DEPTH);
	RCObj.Enable(RenderContext::Mode::RC_DEPTH_TEST);
	RCObj.DepthFunc(RenderContext::Mode::RC_LESS);
	RCObj.DepthFunc(RenderContext::RC_LESS);
	RCObj.ConvertPerspective(60, width / height, 0.1, 1000);
}

void ShadowMap_App::MouseEvent(int key, int state, int x, int y)
{
	widget->MouseEvent(state, key, x, y);
	cout << "x:" << x << ":y" << y << endl;
	if (fstCamara.IsCapture())
	{
		if (state == 0)
		{
			fstCamara.MouseDown(x, y);
		}
		else {
			fstCamara.MouseUp(x, y);
		}
	}
}

void ShadowMap_App::MouseWheelEvent(int button, int dir, int x, int y)
{
}

void ShadowMap_App::KeyEvent(unsigned char ch, int x, int y)
{
	float speed = 1.0;
	if (ch == 'w')
	{
		fstCamara.MoveForwad(speed * 1.0);
	}
	if (ch == 's')
	{
		fstCamara.MoveBack(speed * 1.0);
	}
	if (ch == 'a')
	{
		fstCamara.TurnLeft(speed * 1.0);
	}
	if (ch == 'd')
	{
		fstCamara.TurnRight(speed * 1.0);
	}
	if (ch == 'q')
	{
		fstCamara.TurnUp(speed * 1.0);
	}
	if (ch == 'e')
	{
		fstCamara.TurnDown(speed * 1.0);
	}
	if (ch == 'p')
	{
		widget->SaveToFile("cam.txt");
		fstCamara.Save();
	}
	if (ch == 'r')
	{

		fstCamara.LoadConfig();
	}
	if (ch == 't')
	{
		bUseCam = !bUseCam;
	}
	if (ch == 'y')
	{
		fstCamara.Capture(!fstCamara.IsCapture());
		if (fstCamara.IsCapture())
			cout << "camera is capture" << endl;
		else
			cout << "camera is not capture" << endl;
	}
	cout << ch << endl;
	fstCamara.PrintInfo();
}

void ShadowMap_App::SpecialKeyEvent(int ch, int x, int y)
{
}

void ShadowMap_App::Reshape(int w, int h)
{
	cout << "Reshape:" << w << "*" << h << endl;
	//glViewport(0, 0, w, h);
	RCObj.ViewPort(0, 0, w, h);
	width = w;
	height = h;
	size = vec2(w, h);
}

void ShadowMap_App::MouseMoveEvent(int x, int y)
{
	widget->MouseMoveEvent(x, y);
	fstCamara.MouseMove(x, y);


}


void ShadowMap_App::UpdateUniformParas(IShader* shader, int mode)
{
	float lightWidth = 100;
	//	if (mode == 1)
		//	shader->SetUniformMatrix4fv("model", glm::value_ptr(modelMatrix));
	shader->SetUniform3f("viewPos", viewPos.x, viewPos.y, viewPos.z);
	shader->SetUniform3f("lightPos", vlightPos.x, vlightPos.y, vlightPos.z);
	shader->SetUniformMatrix4fv("view", const_cast<float*>(glm::value_ptr(GetViewMatrix())));

	shader->SetUniformMatrix4fv("proj", glm::value_ptr(proj));
	//shadow
	mat4 lightViewP = glm::perspective(glm::radians(60.f), 1.f, 0.1f, 1000.f);
	//mat4 lightViewP = glm::ortho(-lightWidth, lightWidth,lightWidth, -lightWidth);
	//mat4 lightView = glm::lookAt(lightPos, /*vec3(*centerPos[0], *centerPos[1], *centerPos[2])*/ vec3(0), vec3(0, 0, 1));
	mat4 lightView = glm::lookAt(vlightPos, vec3(0,0,0), vec3(0, 0, -1));
	shader->SetUniformMatrix4fv("lightView", const_cast<float*>(glm::value_ptr(lightViewP * lightView)));
	shader->SetUniform1f("lightMaxDistance", 500);
	shader->SetUniform3f("ambient", 0.2, 0.3, 0.4);
	shader->SetUniform3f("diffuse", 0.6, 0.5, 0.4);
	shader->SetUniform3f("specular", 0.2, 0.3, 0.4);
	shader->SetUniform1f("depthTestValue", *depthTestValue);
	shader->SetUniform1i("mode", mode);
}

