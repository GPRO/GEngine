# GEngine

#### 介绍
Graphics Engine,自己对OpenGL的封装。
Simple Graphics Engine use Opengl 4.5 
<h1 />
<b>Scene With UI</b><br/>
<img src="snapshot/with UI.png" width="45%" height="45%"/><br/>
<b>Projective Texture</b><br/>
<img src="snapshot/ProjectiveTexture.gif" width="30%" height="30%"/><br/>
<b>Depth Texture</b><br/>
<img src="snapshot/DepthTexture.gif" width="30%" height="30%"/><br/>
<b>Obj Model loader</b><br/>
<img src="snapshot/objmodel & toon shading.gif" width="30%" height="30%"/><br/><br/>

<b>Skeleton Model Animation</b><br/>
<img src="snapshot/SkeletonAnimation.gif" width="30%" height="30%"/><br/><br/>
<b>HDR</b><br/>
<img src="snapshot/HDR.gif" width="30%" height="30%"/><br/>
<b>Model Light</b><br/>
<img src="snapshot/model with light.gif" width="30%" height="30%"/><br/><br/>


<table width='300px'>
    <tr><th colspan='2'><b>Bloom对比</b></th></tr>
    <tr><td>原画质</td><td>bloom效果</td></tr>
    <tr><td><img src="snapshot/bloom/原画质.png" /></td>
    <td><img src="snapshot/bloom/bloom.png" /></td></tr>
</table><br/><br/>

<table >
    <tr><td>原画质</td><td>half(一半分辨率后填充)</td><td>FSR(一半分辨率填充后锐化)</td>
    <tr><th colspan='3'>FSR对比1</th></tr>
    <tr>
        <td><img src="snapshot/src.png" /></td>
        <td><img src="snapshot/half.png" /></td>
        <td><img src="snapshot/fsr.png" /></td>
    </tr>
    <tr><th colspan='3'>FSR对比2</th></tr>
    <tr>
        <td><img src="snapshot/src1.png" /></td>
        <td><img src="snapshot/half1.png" /></td>
        <td><img src="snapshot/fsr1.png" /></td>
    </tr>
</table>





