
#version 400
uniform mat4 view;
uniform mat4 trans;
uniform mat4 proj;

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec2 vTex;
layout (location = 2) in vec3 vNorm;

out vec3 n;

void main()
{
	gl_Position = proj *view* trans*vec4(vPosition,1);
	n = vNorm;
}