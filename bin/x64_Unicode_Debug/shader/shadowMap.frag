#version 400

uniform vec3 ambient;
uniform vec3 diffuse;
uniform vec3 specular;
uniform int mode;	//0 shadow 1 normal
uniform float depthTestValue = 1;	//0 shadow 1 normal
uniform sampler2D shadow;

in float depth;
in vec2 tc;
in vec3 L;
in vec3 N;
in float intensity;
in vec3 viewDir;
in vec3 inLightPos;

out vec4 FragColor;

float GetLogedDepth(float depth)
{
	return log(depth/10) / 10.f;
}
vec3 ConvertDepthToVec3(float depth)
{
	vec3 v;
	if (depth >= 100)
	{
		v.r = depth / 1000.f;
		depth -= v.r * 1000.f;
	}
	else
	{
		v.r = 0;
	}
	if (depth >= 10)
	{
		v.g = depth / 100.f;
		depth -= v.g * 100;
	}
	else {
		v.g = 0;
	}
	if (depth < 10)
	{
		v.b = depth / 10;
	}
	else {
		v.b = 0;
	}
	return v;
}
float ConvertVec3ToDepth(vec3 v)
{
	return (v.r * 1000) + (v.g * 100) + (v.b * 10);
}


vec3 EncodeDepth(float d)
{
	return vec3(ConvertDepthToVec3(d));
}
float DecodeDepth(vec3 d)
{
	return ConvertVec3ToDepth(d);
}


void main()
{
	if(mode == 0)
	{

		FragColor = vec4(EncodeDepth(depth), 1);
		return;
	}
	float power = 1;

	float LN =	dot(L, N); 
	LN = LN < 0 ? 0 : LN;

	float insy = intensity < 0 ? 0 : intensity;
	vec3 diffColor =  power * insy  * LN * diffuse;

	float vDN = dot(normalize(viewDir + L), N);
	vDN = vDN < 0 ? 0 : vDN;
	vec3 specColor = specular * power * intensity *  pow(vDN, 5);
	int outOfRange = 0;
	vec2 tcoord = tc;
	if(tcoord.s < -1 || tcoord.s > 1)
	{
		tcoord.s = 0;
		outOfRange = 1;
	}
	if(tcoord.t < -1 || tcoord.t > 1)
	{
		tcoord.t = 0;
		outOfRange = 1;
	}
	if(outOfRange == 0)
	{
		vec2 uv = tcoord.st * 0.5 + vec2(0.5 , 0.5);
		vec4 depthC = texture2D(shadow, uv); 
		float cachedDepth = DecodeDepth(depthC.rgb);
		float d = abs(cachedDepth - depth);
		if(d <= depthTestValue )
		{
			vec3 finalColor = ambient + diffColor + specColor;
			FragColor = vec4(finalColor,1);
		}else{
			FragColor =  vec4(0.2,0.2,0.2,1); 
		}
	}else{
		FragColor = vec4(ambient,1);
	}

}