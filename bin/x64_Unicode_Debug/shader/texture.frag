#version 450

uniform sampler2D tex;
layout (location = 1) in vec3 Normal;
layout (location = 0) in vec2 TexCoord;
layout (location = 2) in vec3 VertexTexCoord;

out vec4 FragColor;

void main()
{

    FragColor = texture2D(tex,TexCoord);
}