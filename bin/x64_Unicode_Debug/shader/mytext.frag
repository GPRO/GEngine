#version 450

uniform sampler2D tex;
//in vec2 TexCoord;
//in vec3 Normal;
//in vec3 Position;
in vec2 tc;
out vec4 FragColor;

void main()
{

    FragColor = texture2D(tex, tc);
}