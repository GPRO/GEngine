
#version 400

in vec2 TexCoord;
in vec3 Normal;
in vec3 Position;

void main()
{
	 gl_Position =  vec4(Position,1);
}