#version 450

out vec4 FragColor;



in vec3 n;

void main()
{
	FragColor = vec4(n,1.0);
}