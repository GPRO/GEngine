
#version 400
uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

in vec3 Pos;
in vec2 TexCoord;
in vec3 Normal;

out vec2 tc;

void main()
{
	 gl_Position =  proj * view * model * vec4(Pos,1); 
	 tc = TexCoord;
}