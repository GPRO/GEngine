#version 450
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec3 viewPos;

uniform vec3 lightPos;
uniform mat4 lightView;
uniform float lightMaxDistance;

uniform int mode;	//0 shadow 1 normal

layout (location = 0)in vec3 pos;
layout (location = 1)in vec2 texCoord;
layout (location = 2)in vec3 norm;

out vec2 tc;
out float depth;
out vec3 L;
out vec3 N;
out vec3 viewDir;
out float intensity;
out vec3 inLightPos;

float GetLogedDepth(float depth)
{
	return log(depth*10) / 10.f;
}

void main()
{
	vec4 pos4 =  model * vec4(pos, 1);
	vec3 transedPos = pos4.xyz / pos4.w;
	vec3 lightToPoint = lightPos - transedPos;
	L = normalize(lightToPoint);
	vec4 inLightPos4 = lightView  * pos4;
	inLightPos = vec3(inLightPos4.xyz) / inLightPos4.w;
	tc = inLightPos4.st /inLightPos4.w; 
	depth = length(lightToPoint);
	N  =  (transpose(inverse(model)) * vec4(normalize(norm),1)).xyz;
	if(mode == 0)
	{	
		gl_Position = inLightPos4;
		return ;
	}
	viewDir = normalize(viewPos - pos4.xyz);
	intensity = 1 - (depth / lightMaxDistance);
	gl_Position = proj * view * pos4;
}
